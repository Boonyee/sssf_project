require('dotenv').config();
// express
const express = require('express');
const app = express();

// Apollo and graphql stuffs
const { ApolloServer } = require('apollo-server-express');
const schemas = require('./schemas/index.js');
const resolvers = require('./resolvers/index.js');

// authentication stuffs
const checkAuth = require('./utils/checkAuth.js');

// Stock data fetching
const fetch = require('isomorphic-fetch');
const getConfig = require('./config');
const { getStocksData, insertStocksData } = require('./utils/queries2.js');

// Database
const db = require('./utils/db');

// Web push notification
const webpush = require('web-push');
webpush.setVapidDetails(
  process.env.WEB_PUSH_CONTACT,
  process.env.VAPID_PUBLIC,
  process.env.VAPID_PRIVATE
);

// timezone helpers
const moment = require('moment-timezone');

// cron job scheduler
const schedule = require('node-schedule');
const eventJob = schedule.scheduleJob('*/5 * * * *', async (fireDate) => {
  console.log('calling eventJob');
  console.log('supposed to be triggered on', fireDate.toISOString());
  await getStocksData(fireDate);
});
// const testJob = schedule.scheduleJob('*/1 * * * *', (fireDate) => {
//   console.log('calling testJob');
//   console.log('supposed to be triggered on', fireDate.toISOString());
// });
console.log('timestamp right now:', new Date().toISOString());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const symbols = ['NFLX'];

app.get('/fetch', async (req, res) => {
  try {
    // (async function getStocksData() {
    const apiConfig = getConfig('finnhubOptions');
    const { host, func, resolution } = apiConfig;
    const insertStockDataPromises = [];
    symbols.forEach(async (symbol) => {
      const response = await (
        await fetch(`
      https://finnhub.io/api/v1/quote?symbol=${symbol}&token=c21c6vaad3idupe6tmv0
      `)
      ).json();
      if (response.error) {
        throw new Error('Error while adding stock data');
      }

      const result = await (
        await fetch(
          `${host}${func}?symbol=${symbol}&resolution=${resolution}&from=${
            response.t - 7600
          }&to=${response.t}&token=c21c6vaad3idupe6tmv0`
        )
      ).json();
      if (result.error) {
        throw new Error(result.error);
      } else if (result.s == 'no_data') {
        console.log('fetch result: no new datapoint');
      } else {
        const dataPoints = result.t.map((val, index) => {
          return {
            symbol,
            time: val * 1000,
            high: result.h[index],
            low: result.l[index],
            open: result.o[index],
            close: result.c[index],
            volume: result.v[index],
          };
        });
        insertStockDataPromises.push(insertStocksData(dataPoints));
      }
    });

    await Promise.all(insertStockDataPromises);
    // console.log("result :>> ", result);
    res.send('fetch successful');
    // })();
  } catch (err) {
    res.send(err);
    throw new Error(err);
  }
});

app.get('/webpush', async (req, res) => {
  try {
    const sub = JSON.parse(
      '{"endpoint":"https://par02p.notify.windows.com/w/?token=BQYAAAC9C88vJ0bz5KRnNywoA21GcLIBSHTQt08ZfZ14uIn7B23VxZz0F9uYgF8eZ1G%2b9jXKFoYGQlDKOMKCZOxGnyc1MxzOgflKt03YItjltTKBbD6UZbXhuxyIcVLdR%2bm7CLNZqGXFCNni6vihasPTjnXxuZqL79tebbYqjtshlRTm%2bwNoH6WXLmTgOuoO%2be7fxPzIHjB%2bj4QjgCvvtUjkiJiSyQMgv0daeYTJDtxKc8tAGByOebINS7J7EAh0lmyyprHZ5zZub4ZP8mSVm3YBdwb76nQ19PpepyjfGshFUtVHHvUFRJEBiA4rlcunI6C64Tk%3d","expirationTime":null,"keys":{"p256dh":"BEwv0idv4SU7nGBzJR9x2NNe8KXWAx_lFdbYH0i0hIWObprsRzw4Jip9n9lar4pYMjxRNYDoQEPe8b1lbfwhc64","auth":"lndB59vIx_Dd5segSdEqKg"}}'
    );
    // console.log('sub :>> ', sub);
    const result = await webpush.sendNotification(
      sub,
      JSON.stringify({
        title: 'Test',
        body: 'seems working!',
      })
    );
    res.send(result);
  } catch (err) {
    res.send(err);
    throw new Error(err);
  }
});

db.on('connected', () => {
  try {
    const server = new ApolloServer({
      typeDefs: schemas,
      resolvers,
      context: async ({ req, res }) => {
        if (req) {
          const user = await checkAuth(req, res);
          // console.log("app", user);
          return {
            req,
            res,
            user,
          };
        }
      },
    });

    server.applyMiddleware({ app });

    // app.listen({ port: process.env.PORT }, () =>
    //   console.log(
    //     `🚀 Server ready at http://localhost:${process.env.PORT}${server.graphqlPath}`
    //   )
    // );
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    if (process.env.NODE_ENV === 'production') {
      console.log('prduction');
      const production = require('./sec/production.js');
      production(app, process.env.PORT_HTTPS);
    } else {
      console.log('localhost');
      const localhost = require('./sec/localhost.js');
      localhost(app, process.env.PORT_HTTPS, process.env.PORT_HTTP);
    }
  } catch (e) {
    // console.log(e);
    throw e;
  }
});
