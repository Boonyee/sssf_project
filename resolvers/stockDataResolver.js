const StockData = require('../models/StockData.js');

module.exports = {
  Query: {
    stock_data: (parent, { order_by, where, limit }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        return where
          ? StockData.find()
              .where('symbol')
              .equals(where.symbol)
              .sort({ time: order_by.time === 'desc' ? -1 : 1 })
              .limit(limit)
          : StockData.find()
              .sort({ time: order_by.time === 'desc' ? -1 : 1 })
              .limit(limit);
      } catch (err) {
        throw err;
      }
    },
  },
  Symbol: {
    async stock_symbol_aggregate(parent) {
      try {
        const max_result = (
          await StockData.aggregate([
            {
              $match: { symbol: parent.id },
            },
            { $sort: { high: -1 } },
            {
              $group: {
                _id: null,
                max_high: { $first: '$high' },
                volume: { $first: '$volume' },
                // max_vol: { $max: "$volume" },
                // min_low: { $min: "$low" },
                // min_vol: { $min: "$volume" },
              },
            },
          ])
        ).pop();
        const min_result = (
          await StockData.aggregate([
            {
              $match: { symbol: parent.id },
            },
            { $sort: { low: 1 } },
            {
              $group: {
                _id: null,
                min_low: { $first: '$low' },
                volume: { $first: '$volume' },

                // max_vol: { $max: "$volume" },
                // min_low: { $min: "$low" },
                // min_vol: { $min: "$volume" },
              },
            },
          ])
        ).pop();
        // console.log('max_result :>> ', max_result);
        // console.log('min_result :>> ', min_result);

        return {
          max: {
            high: max_result.max_high,
            volume: max_result.volume,
          },
          min: {
            low: min_result.min_low,
            volume: min_result.volume,
          },
        };
      } catch (err) {
        throw err;
      }
    },
  },
  Mutation: {
    addStockData: (parent, { objects }) => {
      return StockData.create(objects);
    },
  },
};
