require('dotenv').config();
const { AuthenticationError } = require('apollo-server-errors');
const webpush = require('web-push');

const User = require('../models/User.js');
const jwt = require('jsonwebtoken');
const passport = require('../utils/pass.js');
const Symbol = require('../models/Symbol.js');
const fetch = require('isomorphic-fetch');
const StockData = require('../models/StockData.js');

//const passport = require('../utils/pass.js');

const addStockData = async (symbol, token) => {
  try {
    const response = await (
      await fetch(`
    https://finnhub.io/api/v1/quote?symbol=${symbol}&token=${token}
    `)
    ).json();
    if (response.error) {
      throw new Error('Error while fetch stock quote');
    }

    const result = await (
      await fetch(
        `https://finnhub.io/api/v1/stock/candle?symbol=${symbol}&resolution=5&from=${
          response.t - 7600
        }&to=${response.t}&token=${token}`
      )
    ).json();
    if (result.error || result.s === 'no_data') {
      throw new Error('Error while fetching candlestick data');
    } else {
      const dataPoints = result.t.map((val, index) => {
        return {
          symbol,
          time: val * 1000,
          high: result.h[index],
          low: result.l[index],
          open: result.o[index],
          close: result.c[index],
          volume: result.v[index],
        };
      });
      await StockData.create(dataPoints);
    }
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports = {
  JobEnum: {
    Software_Developer: 'Software Developer',
  },
  Events: {
    user(parent) {
      try {
        return User.findOne({ _id: parent.user_id });
      } catch (err) {
        throw err;
      }
    },
  },
  Mutation: {
    signup: async (parent, { object }) => {
      try {
        // console.log('object :>> ', object);
        const user = (await User.create(object)).toObject();
        const token = jwt.sign(user, process.env.JWT_SECRET);
        return { token, user: { ...user, password: undefined } };
      } catch (err) {
        if (err.name === 'MongoError' && err.code === 11000) {
          throw new Error('User or API token already exist');
        }

        // Some other error
        throw err;
      }
    },
    login: async (parent, args, { req, res }) => {
      // call passport login (done in class)
      try {
        return await new Promise((resolve, reject) => {
          passport.authenticate(
            'local',
            { session: false },
            (err, user, info) => {
              if (err || !user) {
                console.log('info :>> ', info);
                resolve(new AuthenticationError('login failed'));
              }
              const token = jwt.sign(user, process.env.JWT_SECRET);
              resolve({
                user,
                token,
              });
            }
          )({ body: args }, res);
        });
      } catch (err) {
        throw err;
      }
    },
    insert_subscription: async (parent, { object }, { user }) => {
      try {
        // console.log('id :>> ', objects.id);
        // console.log('subscription :>> ', objects.subscription);
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        const webpushPayload = {
          title: `Hello ${user.email}!`,
          body: `Push notification is ready`,
        };
        const sendResult = await webpush.sendNotification(
          JSON.parse(object.subscription),
          JSON.stringify(webpushPayload)
        );
        console.log('sendResult', sendResult);
        if (sendResult.statusCode === 201) {
          const result = await User.findByIdAndUpdate(
            object.id,
            { $addToSet: { subscription: object.subscription } },
            { new: true }
          );
          return result;
        }
        // console.log('result', result);
      } catch (err) {
        throw err;
      }
    },
    subscribe_stock: async (parent, { user_id, symbol }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        const symbols = (await Symbol.find({}).exec()).map((ele) => ele.id);
        const user_symbols = (
          await User.findById(user_id).populate('symbol').exec()
        ).symbol.map((ele) => ele.id);

        const token = (await User.findById(user_id).exec()).key;
        const symbol_included = symbols.includes(symbol);
        const user_included = user_symbols.includes(symbol);

        if (symbol_included && !user_included) {
          const found = (await Symbol.findOne({ id: symbol }).exec())._id;
          const result = await User.findByIdAndUpdate(
            user_id,
            { $addToSet: { symbol: found } },
            { new: true }
          );
          return result;
        } else if (!symbol_included) {
          const response = await (
            await fetch(`
            https://finnhub.io/api/v1/stock/profile2?symbol=${symbol}&token=${token}
            `)
          ).json();
          if (!response.error && response.name) {
            // if found company profile
            // 1st, add stock data
            // 2nd, add symbol info
            // 3nd, add symbol to user
            await addStockData(symbol, token);
            const new_symbol = await Symbol.create({
              id: symbol,
              company: response.name,
            });
            const result = await User.findByIdAndUpdate(
              user_id,
              { $addToSet: { symbol: new_symbol._id } },
              { new: true }
            );

            return result;
          } else {
            throw new Error('API Error: error while fetching data');
          }
        } else {
          throw new Error(
            'Found symbol in User but not in Symbol or already subscribed'
          );
        }
      } catch (err) {
        throw err;
      }
    },
    unsubscribe_stock: async (parent, { user_id, symbol }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        const symbolObject = await Symbol.findOne({ id: symbol });
        if (symbolObject) {
          const result = await User.findByIdAndUpdate(
            user_id,
            {
              $pull: { symbol: symbolObject._id },
            },
            {
              new: true,
            }
          );
          // console.log('result :>> ', result);
          return result;
        } else {
          throw new Error('Symbol not found!!!');
        }
      } catch (err) {
        console.log(err);
        throw err;
      }
    },
  },
};
