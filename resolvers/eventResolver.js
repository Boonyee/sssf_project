const Events = require('../models/Events.js');
const { AuthenticationError } = require('apollo-server-errors');

module.exports = {
  Query: {
    events: (parent, { filters }) => {
      try {
        const filter = JSON.stringify({
          ...filters,
          trigger_value_gte: undefined,
          trigger_value: filters.trigger_value_gte
            ? { $gte: filters.trigger_value_gte }
            : undefined,
        });
        // console.log('filter ', JSON.parse(filter));
        return Events.find(JSON.parse(filter));
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Mutation: {
    insert_events: (parent, { objects }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        return Events.create(objects);
      } catch (err) {
        throw new Error(err);
      }
    },
    update_events: (parent, { id, set }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        return Events.findByIdAndUpdate(id, { $set: set });
      } catch (err) {
        throw new Error(err);
      }
    },
    delete_events: async (parent, { id }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }
        return await Events.findByIdAndDelete(id);
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Symbol: {
    symbol_events(parent, args) {
      try {
        return Events.find({ ...args, symbol: parent.id });
      } catch (err) {
        throw new Error(err);
      }
    },
  },
};
