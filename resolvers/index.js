const stockDataResolver = require('./stockDataResolver.js');
const {
  DateTimeResolver: DateTime,
  EmailAddressResolver: EmailAddress,
} = require('graphql-scalars');
const symbolResolver = require('./symbolResolver.js');
const eventResolver = require('./eventResolver.js');
const userResolver = require('./userResolver.js');

module.exports = [
  stockDataResolver,
  symbolResolver,
  eventResolver,
  userResolver,
  { DateTime },
  { EmailAddress },
];
