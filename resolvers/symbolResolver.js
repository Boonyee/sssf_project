const User = require('../models/User.js');
const Symbol = require('../models/Symbol.js');
const { AuthenticationError } = require('apollo-server-errors');

module.exports = {
  Query: {
    symbol: async (parent, { user_id }, { user }) => {
      try {
        if (!user) {
          throw new AuthenticationError('Unauthorized');
        }

        const doc = await User.findById(user_id).populate('symbol').exec();

        if (doc) {
          return doc.symbol;
        }
      } catch (err) {
        throw err;
      }
    },
  },
};
