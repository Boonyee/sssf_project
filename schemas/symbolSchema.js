const { gql } = require('apollo-server-express');

module.exports = gql`
  type Symbol {
    id: String!
    company: String!
    symbol_events(user_id: String): [Events]
    stock_symbol_aggregate: StockDataAggregate
  }

  extend type Query {
    symbol(user_id: String): [Symbol]
    stock_symbol_aggregate: StockDataAggregate
  }

  type StockDataAggregate {
    max: StockDataMaxFields
    min: StockDataMinFields
  }

  # aggregate max on columns
  type StockDataMaxFields {
    high: Float
    volume: Float
  }

  # aggregate min on columns
  type StockDataMinFields {
    low: Float
    volume: Float
  }
`;
