const stockDataSchema = require('./stockDataSchema.js');
const {
  DateTimeTypeDefinition,
  EmailAddressTypeDefinition,
} = require('graphql-scalars');
const symbolSchema = require('./symbolSchema.js');
const eventSchema = require('./eventSchema.js');
const userSchema = require('./userSchema.js');

const { gql } = require('apollo-server-express');

const linkSchema = gql`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`;

module.exports = [
  linkSchema,
  stockDataSchema,
  symbolSchema,
  eventSchema,
  userSchema,
  DateTimeTypeDefinition,
  EmailAddressTypeDefinition,
];
