const { gql } = require('apollo-server-express');

module.exports = gql`
  type Events {
    _id: ID!
    symbol: String!
    trigger_type: TriggerType!
    trigger_value: Float!
    user_id: String!
    user: User!
  }

  extend type Query {
    events(filters: EventFiltersInput): [Events]
  }

  input EventFiltersInput {
    user_id: String
    symbol: String
    trigger_type: TriggerType
    trigger_value_gte: Float
  }

  enum TriggerType {
    time
    event
  }

  extend type Mutation {
    insert_events(objects: AddEventInput): Events
    update_events(id: ID, set: UpdateEventInput): Events
    delete_events(id: ID): Events
  }

  input AddEventInput {
    symbol: String
    user_id: String
    trigger_type: String
    trigger_value: Float
  }

  input UpdateEventInput {
    trigger_type: String
    trigger_value: Float
  }
`;
