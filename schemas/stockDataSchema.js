const { gql } = require('apollo-server-express');

module.exports = gql`
  type StockData {
    id: ID!
    symbol: String!
    high: Float!
    low: Float!
    open: Float!
    close: Float!
    volume: Float!
    time: DateTime!
  }

  extend type Query {
    stock_data(
      order_by: OrderInput
      where: WhereInput
      limit: Int = 25
    ): [StockData]
  }

  input WhereInput {
    symbol: String
  }

  extend type Mutation {
    addStockData(objects: [addStockInput!]!): [StockData]
  }

  input addStockInput {
    symbol: String!
    high: Float!
    low: Float!
    open: Float!
    close: Float!
    volume: Float!
    time: Float!
  }

  input OrderInput {
    time: TimeEnum!
  }

  enum TimeEnum {
    desc
    aesc
  }
`;
