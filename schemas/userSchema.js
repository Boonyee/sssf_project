const { gql } = require('apollo-server-express');

module.exports = gql`
  type User {
    _id: ID!
    email: EmailAddress!
    job: JobEnum!
    organization: String!
    key: String!
    subscription: [String]
  }

  type AuthPayload {
    token: String
    user: User
  }

  extend type Mutation {
    signup(object: UserObject!): AuthPayload
    login(email: String!, password: String!): AuthPayload
    insert_subscription(object: UserSubInput): User
    subscribe_stock(user_id: String!, symbol: String!): User
    unsubscribe_stock(user_id: String!, symbol: String!): User
  }

  input UserObject {
    email: EmailAddress!
    password: String!
    job: JobEnum!
    organization: String!
    key: String!
  }

  enum JobEnum {
    Investor
    Software_Developer
    Educator
    Student
    Other
  }

  input UserSubInput {
    id: String
    subscription: String
  }
`;
