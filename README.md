# Stock Price Notifier API

This is the API server for "Stock Price Notifier" application. The server is powered by Apollo GraphQL server
and Finnhub API

### How do I get set up?

First you need to register to [finnhub.io](finnhub.io) to get API key

```
npm install
```

### DOCUMENTATION

ENDPOINT: /graphql

Before using API, login or signup first to get token. Every other queries and mutations need token

- login

```
mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        _id
        email
        key
      }
    }
  }
```

- signup

```
enum JobEnum {
    Investor
    Software_Developer
    Educator
    Student
    Other
  }

mutation signup(
    $email: EmailAddress!
    $password: String!
    $job: JobEnum!
    $organization: String!
    $key: String!
  ) {
    signup(
      object: {
        email: $email
        password: $password
        job: $job
        organization: $organization
        key: $key
      }
    ) {
      token
      user {
        _id
        email
        key
      }
    }
  }
```

- get symbols subscribed by user

```
query getSymbols($userId: String) {
    symbol(user_id: $userId) {
      id
      company
      symbol_events(user_id: $userId) {
        _id
        symbol
        trigger_type
        trigger_value
      }
      stock_symbol_aggregate {
        max {
          high
          volume
        }
        min {
          low
          volume
        }
      }
    }
  }
```

- get stock data

```
query getStocksData($symbol: String) {
    stock_data(
      order_by: { time: desc }
      where: { symbol: $symbol }
      limit: 25
    ) {
      high
      low
      open
      close
      volume
      time
    }
  }
```

- record push subscription object by user
  the subscription object need to be valid: can send push notification back to endpoint
  see [ServiceWorkerContainer.ready](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/ready)

```
mutation userSubscription($userId: String, $subscription: String) {
    insert_subscription(object: { id: $userId, subscription: $subscription }) {
      _id
    }
  }
```

- add event to either stock price, or notify every hour

```
mutation addEvent(
    $symbol: String
    $user_id: String
    $triggerType: String
    $triggerValue: Float
  ) {
    insert_events(
      objects: {
        symbol: $symbol
        user_id: $user_id
        trigger_type: $triggerType
        trigger_value: $triggerValue
      }
    ) {
      _id
    }
  }
```

- update event by user

```
mutation updateEvents($id: ID, $triggerType: String, $triggerValue: Float) {
    update_events(
      id: $id
      set: { trigger_type: $triggerType, trigger_value: $triggerValue }
    ) {
      _id
      trigger_type
      trigger_value
    }
  }
```

- delete event

```
mutation clearEvent($id: ID!) {
    delete_events(id: $id) {
      _id
    }
  }
```

- subscribe to stock by symbol

```
mutation subscribe($user_id: String!, $symbol: String!) {
    subscribe_stock(user_id: $user_id, symbol: $symbol) {
      _id
    }
  }
```

- unsubscribe to stock symbol

```
mutation unsubscribe($user_id: String!, $symbol: String!) {
    unsubscribe_stock(user_id: $user_id, symbol: $symbol) {
      _id
    }
  }
```

### How does it work?

The server has been set up to called cron job every 5 minutes to:

1. fetch new data by symbols that all user has been subscribed
2. if the data meet event condition, sent push notification back to user

### Who do I talk to?

- email me: weerapat.tec@hotmail.com
