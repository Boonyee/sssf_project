const mongoose = require('mongoose');
require('mongoose-uuid4')(mongoose);
const UUID = mongoose.SchemaTypes.UUID;

const eventSchema = new mongoose.Schema({
  symbol: { type: String, ref: 'Symbol' },
  user_id: { type: mongoose.SchemaTypes.ObjectId, ref: 'User' },
  trigger_type: { type: String, enum: ['time', 'event'], required: true },
  trigger_value: { type: Number, default: 1 },
});

module.exports = mongoose.model('Event', eventSchema, 'events');
