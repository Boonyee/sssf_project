const mongoose = require("mongoose");

const stockDataSchema = new mongoose.Schema({
  symbol: { type: String, ref: "Symbol" },
  high: String,
  low: String,
  open: String,
  close: String,
  volume: String,
  time: Date,
});

module.exports = mongoose.model("StockData", stockDataSchema, "stockdata");
