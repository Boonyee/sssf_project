const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRound = 12;
const { isEmail } = require('validator');
const fetch = require('isomorphic-fetch');

const tokenValidator = async (token) => {
  const result = await (
    await fetch(`https://finnhub.io/api/v1/quote?symbol=NFLX&token=${token}`)
  ).json();
  return result.error ? false : true;
};

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: [isEmail, 'invalid email'],
  },
  password: {
    type: String,
    required: true,
  },
  job: {
    type: String,
    enum: ['Investor', 'Software Developer', 'Educator', 'Student', 'Other'],
    required: true,
  },
  symbol: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Symbol' }],
  organization: { type: String, default: 'none' },
  subscription: [{ type: String }],
  key: {
    type: String,
    unique: true,
    required: true,
    validate: {
      validator: tokenValidator,
      message: 'invalid key',
    },
  },
});

userSchema.pre('save', async function (next) {
  // only hash the password if it has been modified (or is new)
  try {
    if (!this.isModified('password')) return next();

    // generate a salt
    const salt = await bcrypt.genSalt(saltRound);
    // hash the password using our new salt
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    next();
  } catch (err) {
    next(err);
  }
});

module.exports = mongoose.model('User', userSchema, 'user');
