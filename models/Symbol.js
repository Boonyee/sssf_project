const mongoose = require('mongoose');

const symbolSchema = new mongoose.Schema({
  id: { type: String, unique: true, required: true },
  company: { type: String, required: true },
});

module.exports = mongoose.model('Symbol', symbolSchema, 'symbol');
