require('dotenv').config();
const https = require('https');
const http = require('http');
const fs = require('fs');

const sslkey = fs.readFileSync(__dirname + '\\..\\ssl-key.pem');
const sslcert = fs.readFileSync(__dirname + '\\..\\ssl-cert.pem');

const options = {
  key: sslkey,
  cert: sslcert,
};

const httpsRedirect = (req, res) => {
  res.writeHead(301, {
    Location: `https://localhost:${process.env.PORT_HTTPS}` + req.url,
  });
  res.end();
};

const localhost = (app, httpsPort, httpPort) => {
  https.createServer(options, app).listen(httpsPort);
  http.createServer(httpsRedirect).listen(httpPort);
};

module.exports = localhost;
