'use strict';
require('dotenv').config();
const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const User = require('../models/User.js');
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const bcrypt = require('bcrypt');

// local strategy for username password login
passport.use(
  new Strategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      try {
        const user = await User.findOne({ email });
        // console.log('Local strategy', user);
        if (user === undefined) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!(await bcrypt.compare(password, user.password))) {
          return done(null, false, { message: 'Wrong cretendials.' });
        }
        return done(null, user.toJSON(), { message: 'Logged In Successfully' });
      } catch (err) {
        return done(err);
      }
    }
  )
);

// TODO: JWT strategy for handling bearer token
passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    },
    async (jwtPayload, done) => {
      try {
        const user = await User.findById(jwtPayload._id);
        // console.log("JWT strategy", user);
        if (user == null) {
          return done(null, false, { message: 'User not found.' });
        }

        return done(null, user, { message: 'Logged In Successfully' });
      } catch (err) {
        return done(err);
      }
    }
  )
);

module.exports = passport;
