require('dotenv').config();
const getConfig = require('../config');
const { createApolloFetch } = require('apollo-fetch');
const webpush = require('web-push');
const isoFetch = require('isomorphic-fetch');
const moment = require('moment-timezone');
const StockData = require('../models/StockData.js');

// get key and symbols for each user
// require Symbol model to prevent populate error
const User = require('../models/User.js');
const Symbol = require('../models/Symbol.js');

webpush.setVapidDetails(
  process.env.WEB_PUSH_CONTACT,
  process.env.VAPID_PUBLIC,
  process.env.VAPID_PRIVATE
);

const GRAPHQL_URL = getConfig('graphqlURL');
const fetch = createApolloFetch({
  uri: GRAPHQL_URL,
});

const symbols = ['NFLX'];

const insertStocksData = async (payload) => {
  try {
    const insertStockMutation = await fetch({
      query: `mutation insertStockData($objects: [addStockInput!]!) {
        addStockData (objects: $objects) {
            id
            time
        }
      }`,
      variables: {
        objects: payload,
      },
    });
    console.log(JSON.stringify(insertStockMutation));
    return insertStockMutation;
  } catch (err) {
    throw err;
  }
};

// web-push part
const getSubscribedUsers = (user_id, symbol, triggerValue) => {
  try {
    return fetch({
      query: `query getSubscribedUsers($symbol: String, $triggerValue: Float, $user_id: String) {
          events(filters: {user_id: $user_id, symbol: $symbol, trigger_type: event, trigger_value_gte: $triggerValue}) {
            user_id
            user {
              subscription
            }
          }
        }`,
      variables: {
        user_id,
        symbol,
        triggerValue,
      },
    });
  } catch (err) {
    throw err;
  }
};
const getSubscribedUsersHourly = (user_id, symbol) => {
  try {
    return fetch({
      query: `query getSubscribedUsers($symbol: String, $user_id: String) {
          events(filters: {user_id: $user_id,symbol: $symbol, trigger_type: time}) {
            user_id
            user {
              subscription
            }
          }
        }`,
      variables: {
        user_id,
        symbol,
      },
    });
  } catch (err) {
    throw err;
  }
};

const checkEvent = async (user_id, symbol, triggerValue) => {
  try {
    const subscribedUsers = (
      await getSubscribedUsers(user_id, symbol, triggerValue)
    ).data.events;
    // console.log('subscribedUsers :>> ', subscribedUsers);
    const webpushPayload = {
      title: `${symbol} - Event Update`,
      body: `The price of this stock is ${triggerValue}`,
    };
    subscribedUsers.forEach((data) => {
      try {
        data.user.subscription.forEach(async (subObj) => {
          const result = await webpush.sendNotification(
            JSON.parse(subObj),
            JSON.stringify(webpushPayload)
          );

          console.log('result', result);
        });
      } catch (err) {
        throw err;
      }
    });
  } catch (err) {
    throw err;
  }
};

const checkEventHourly = async (user_id, symbol, newClose) => {
  try {
    const subscribedUsers = (await getSubscribedUsersHourly(user_id, symbol))
      .data.events;
    console.log('subscribedUsers :>> ', subscribedUsers);
    const webpushPayload = {
      title: `${symbol} - Hourly Update`,
      body: `The price of this stock is ${newClose}`,
    };
    subscribedUsers.forEach((data) => {
      try {
        data.user.subscription.forEach(async (subObj) => {
          const result = await webpush.sendNotification(
            JSON.parse(subObj),
            JSON.stringify(webpushPayload)
          );

          console.log('result', result);
        });
      } catch (err) {
        throw err;
      }
    });
  } catch (err) {
    throw err;
  }
};

const sendWebpush = (subscription, webpushPayload) => {
  try {
    webpush.sendNotification(subscription, webpushPayload);
  } catch (err) {
    throw err;
  }
};

const getStocksData = async (fireDate) => {
  try {
    const userObjects = await User.find({}, 'key symbol')
      .populate('symbol')
      .exec();
    // console.log('userObject :>> ', userObject);

    const apiConfig = getConfig('finnhubOptions');
    const { host, func, resolution } = apiConfig;
    const insertStockDataPromises = [];

    userObjects.forEach((user) => {
      user.symbol.forEach(async ({ id: symbol }) => {
        const result = await (
          await isoFetch(
            `${host}${func}?symbol=${symbol}&resolution=${resolution}&from=${
              Math.floor(fireDate.getTime() / 1000) - 300
            }&to=${Math.floor(fireDate.getTime() / 1000)}&token=${user.key}`
          )
        ).json();
        if (result.error) {
          throw new Error(result.error);
        } else if (result.s == 'no_data') {
          console.log('fetch result: no new datapoint');
        } else {
          const time = result.t.pop() * 1000;
          const high = result.h.pop();
          const low = result.l.pop();
          const open = result.o.pop();
          const close = result.c.pop();
          const volume = result.v.pop();
          const doc = await StockData.findOne({
            time,
            symbol,
          }).exec();
          if (doc) {
            console.log('found datapoint in the database, no insert');
          } else {
            const latestData = [
              {
                symbol,
                high,
                low,
                open,
                close,
                volume,
                time,
              },
            ];
            insertStockDataPromises.push(insertStocksData(latestData));
            if (fireDate.getMinutes() == 0) {
              console.log('calling checkEventHourly');
              await checkEventHourly(user._id, symbol, close);
              await checkEvent(user._id, symbol, close);
            } else {
              console.log('calling checkEvent');
              await checkEvent(user._id, symbol, close);
            }
          }
        }
      });
    });
    await Promise.all(insertStockDataPromises);
  } catch (err) {
    throw err;
  }
};
module.exports = {
  insertStocksData,
  sendWebpush,
  checkEvent,
  checkEventHourly,
  getStocksData,
};
