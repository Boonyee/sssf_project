require('dotenv').config();
const config = {
  databaseOptions: {
    user: '<DATABASE_USER>',
    password: '<DATABASE_PASSWORD>',
    host: '<DATABASE_HOST>',
    port: 5432,
    database: '<DATABASE_NAME>',
    ssl: true,
  },
  apiHostOptions: {
    host: 'https://www.alphavantage.co/',
    timeSeriesFunction: 'TIME_SERIES_INTRADAY',
    interval: '5min',
  },
  finnhubOptions: {
    host: 'https://finnhub.io/api/v1',
    func: '/stock/candle',
    resolution: '5',
  },
  graphqlURL: `http://localhost:${process.env.PORT}/graphql`,
};

const getConfig = (key) => {
  return config[key];
};

module.exports = getConfig;
